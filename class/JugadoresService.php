<?php

namespace clases;

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/EstructuraService.php';

/** 
 * Class JugadoresService
 * @package clases
 */

use clases\EstructuraService;

class JugadoresService extends EstructuraService
{

  //constantes generales:
  const CT = 't';
  const CTOKEN = 'token';
  const CDATOS = 'datos';
  private $url_endpoint = "";

  // Constructor
  public function __construct()
  {
    include CONFIG . '/variables.php';
    $this->url_endpoint = $endpoint_jugadores;
  }

  /**
   * Método encargado de consumir el servicio que consulta las posiciones del jugador
   *  
   * @return Array
   */
  public function posicionesJugador()
  {
    $token = $this->getToken(EstructuraService::USER,  EstructuraService::P4SSW0RD, $this->url_endpoint);
    $datos = [
      self::CTOKEN => $token,
      self::CT => "posicionesJugador"
    ];

    return json_decode(json_encode($this->cUrl($datos, $this->url_endpoint)), true);
  }
}
