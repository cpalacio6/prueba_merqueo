<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/Jugadores.php';
require_once CLASES . '/JugadoresService.php';
require_once CLASES . '/EquiposService.php';

use clases\Jugadores;
use clases\JugadoresService;
use clases\EquiposService;

$ju = new Jugadores();
$juS = new JugadoresService();
$eq = new EquiposService();

//Se consume el servicio para traer la posición del jugador
$posicion_jugador = $juS->posicionesJugador();

$error_servicio_pj = $posicion_jugador['error'];

if($error_servicio_pj == 1){
    die($posicion_jugador['mensaje']);
}else{
    $tr_posicion = "";

    foreach ($posicion_jugador['datos']['resp'] as $key => $value) {
        $id = $value['id'];
        $posicion = $value['posicion'];
        $tr_posicion = $tr_posicion . "<tr><td>$id</td><td>$posicion</td></tr>";
    }
}

$equipos = $eq->equipos();
$error_servicio_eq = $equipos['error'];

if($error_servicio_eq == 1){
    die($equipos['mensaje']);
}else{
    $tr_pais = "";

    foreach ($equipos['datos']['resp'] as $key2 => $value2) {
        $id2 = $value2['id'];
        $nombre_pais = $value2['nombre_pais'];
        $tr_pais = $tr_pais . "<tr><td>$id2</td><td>$nombre_pais</td></tr>";
    }
}

?>

<div style="position: relative;
    top: 20%;
    left: 40%;
    float: left;">

<table border="1">
    <tr>
        <td>ID</td>
        <td>POSICION</td>
    </tr> 
    <?= $tr_posicion ?>
</table>

</div>

<div style="position: relative; top: 20%; float: right; right: 30%;">

<table border="1">
    <tr>
        <td>ID</td>
        <td>PAIS</td>
    </tr> 
    <?= $tr_pais ?>
</table>

</div>

<hr>

<form method="POST" action="index.php" enctype="multipart/form-data">
<input type="hidden" name="excel" id="excel" value="1">
    <table>
        <tr>
            <td>Subir Excel: </td>
            <td><input type="file" name="uploadedFile" /></td>
        </tr> 
        <tr>
            <td colspan="2"><input type="submit" name="btn_form" value="Cargar" /></td>
        </tr>
    </table>
</form>

<a href="index.php">Regresar al menu</a>

<div style="position:relative; top:10%;">

<?php
if(isset($_POST['excel']) && $_POST['excel'] == 1){
    $datosExcel = $ju->subirArchivo($_POST, $_FILES, 'uploads/', 2);
    echo $datosExcel;
}
?>

</div>

