<?php

namespace clases;

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/LogWS.php';
require_once CLASES . '/TokenJWT.php';
require_once CLASES . '/BaseDatos.php';

use clases\BaseDatos;
use clases\LogWS;
use clases\TokenJWT;

/**
 * Class GestionCredito
 * @package clases
 */
class EstructuraService extends BaseDatos
{

    //constantes generales
    const CNOMBRE = 'nombre';
    const CTIPO = 'tipo';
    const CFINAL = 'final';
    const MENSAJE = 'mensaje';
    const USER = "admin";
    const P4SSW0RD = "El1m1n@t0r1@Wund1@l";

    //constructor
    function __construct()
    {
    }

    /**
     * Función encargada de retornar la respuesta del servicio y guardar los logs
     * 
     * @param Int $error
     * @param String $error
     * @param Array $datos
     * @param String $error
     * @param String $error
     *  
     * @return Json
     */
    function response($error, $mensaje, $datos, $tipo)
    {
        if ($error == 0) {
            header("HTTP/1.1 200 OK");
        } else {
            header("HTTP/1.0 500 Internal Server Error");
        }

        header("Content-Type:application/json; charset=UTF-8");

        $response['error'] = $error;
        $response['mensaje'] = $mensaje;
        $response['datos'] = $datos;

        $json_response = json_encode($response);
        $method = $_SERVER['REQUEST_METHOD'];

        $tipo = trim(addslashes($tipo));

        if ($tipo == "") {
            $tipo = "NOSERVICIO";
        }

        $request = json_encode($_POST);
        $request = addslashes($request);

        $log = new LogWS();
        $log->creaLog($method . ' - ' . $tipo, $request, addslashes($json_response));

        echo $json_response;
    }

    /**
     * Función encargada de validar los token y de responder el servicio si este 
     * no tiene un token viable
     * 
     * @param Array $datos
     *  
     * @return Boolean
     */
    public function validaToken($datos)
    {

        //Parámetros recibidos
        $token = $datos['token'];
        $tipo = $datos[$this::CTIPO];
        $nombre = isset($datos[$this::CNOMBRE]) ? $datos[$this::CNOMBRE] : "$tipo - TOKEN INVALIDO";

        //se instancia la clase
        $tk = new TokenJWT();

        //validamos el token
        $resCkTk = $tk->Check($token);

        //se valida si el tokenno es válido
        if (is_string($resCkTk)) {
            $resp = ['detail' => $resCkTk];
            $this->response(1, 'Fail', $resp, $nombre, $tipo);
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $password
     * @return string
     */
    public function create_hash($password)
    {
        return base64_encode(md5($password));
    }

    /**
     * @param $user
     * @param $hash
     * @param $pass
     * @return array
     */
    public function validarUserPass($user, $hash, $pass)
    {
        if (trim($user) == '' || trim($pass) == '') {
            return [
                self::CFINAL => "ERROR",
                self::MENSAJE => "Por favor, llenar los campos usuario y contraseña.",
            ];
        } else {
            $resp = $this->query("SELECT * FROM usuarios_servicios WHERE user = '$user' AND 
            pass = '$hash' AND activo = 1;");

            if (!empty($resp)) {
                return [self::CFINAL => "OK", self::MENSAJE => ""];
            } else {
                return [self::CFINAL => "ERROR", self::MENSAJE => "Usuario o contraseña son incorrectos."];
            }
        }
    }

    public function getUrl($link)
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $port_local = (empty($_SERVER['SERVER_PORT']) ? '' : ':' . $_SERVER['SERVER_PORT']);
        $url_actual = $protocol . $_SERVER['SERVER_NAME'] . $port_local;

        //obtener url actual 
        return $url_actual . $link;
    }

    /**
     * @param $user
     * @param $password
     * @param $url
     * @return string|null
    */
    protected function getToken($user, $password, $url)
    {
        $intentos = 0;
        $token = "";
        while ($token == "") {
            $array = [
                't' => 'getToken',
                'user' => $user,
                'pass' => $password
            ];

            $token = $this->cUrl($array, $url)->datos->token;

            if (isset($token) && $token != "") {
                return $token;
            }

            $intentos++;
            sleep(1);
            if ($intentos >= 3) {
                return null;
            }
        }
    }

    /**
     * @param $array
     * @param $url
     * @return mixed
     */
    protected function cUrl($array, $url)
    {

        //se agrega el usuario de la sesión si este existe
        $array['usuario_ip'] = base64_encode($this->obtenerIP());

        $array = http_build_query($array, '', '&');

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_ENCODING => "",
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $array,
            CURLOPT_SSL_VERIFYHOST =>  0,
            CURLOPT_SSL_VERIFYPEER => false,
        ));

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($this->cleanString($result));
    }

    private function cleanString($val)
    {
        $non_displayables = array(
            '/%0[0-8bcef]/',            # url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             # url encoded 16-31
            '/[\x00-\x08]/',            # 00-08
            '/\x0b/',                   # 11
            '/\x0c/',                   # 12
            '/[\x0e-\x1f]/',            # 14-31
            '/x7F/'                     # 127
        );
        foreach ($non_displayables as $regex) {
            $val = preg_replace($regex, '', $val);
        }
        $search = array("\0", "\r", "\x1a", "\t");
        $data = trim(str_replace($search, '', $val));

        if (substr($data, 0, 3) == chr(hexdec('EF')) . chr(hexdec('BB')) . chr(hexdec('BF'))) {
            return substr($data, 3);
        } else {
            return $data;
        }
    }
}
