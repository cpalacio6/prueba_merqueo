<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/TokenJWT.php';
require_once CLASES . '/LogWS.php';
require_once CLASES . "/Validaciones.php";
require_once CLASES . '/EstructuraService.php';
require_once CLASES . '/Jugadores.php';

use clases\LogWS;
use clases\TokenJWT;
use clases\Validaciones;
use clases\EstructuraService;
use clases\Jugadores;

//constantes tipos de datos
const CINT = 'int';
const CSTRING = 'string';
const CBOOL = 'bool';
const CDOUBLE = 'double';
const CDATE = 'date';

//constantes generales
const FAIL = "Fail";
const DETAIL = "detail";
const CTOKEN = "token";
const COK = 'OK';
const CMSJ = 'mensaje';
const CDATOS = 'datos';
const CESTADO = 'estado';
const CRESP = 'resp';
const CTIPO = 'tipo';
const CTIPOVARIBLEINVALIDA = 'TIPO VARIABLE INVÁLIDA';

$method = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
$tipo = $_POST["t"];
$token = isset($_POST[CTOKEN]) ? $_POST[CTOKEN] : "";

$datos_valida_token = [
    CTOKEN => $token,
    CTIPO => $tipo
];

//valor por defecto de error
$error = 1;
$log = new LogWS();
if (false) {
    $resCkTk = "IP invalida.";
    $str->response($error, $resCkTk, [], "IP INVALIDA", $tipo);
    die();
}

$val = new Validaciones();
$tk = new TokenJWT();
$str = new EstructuraService();

header("Content-Type:application/json; charset=UTF-8");
if ($method == 'POST') {
    switch ($tipo) {

        case "getToken":
            $user = isset($_POST["user"]) ? $_POST["user"] : "";
            $pass = isset($_POST["pass"]) ? $_POST["pass"] : "";

            if ($user == "" || $pass == "") {
                $resp = [DETAIL => "Invalid user logged in."];
                $resCkTk = FAIL;
                $str->response($error, $resCkTk, $resp, $tipo);
            } else {
                $hash = $str->create_hash($pass);
                $resp = $str->validarUserPass($user, $hash, $pass);

                if ($resp["final"] == COK) {
                    $token = $tk->SignIn();
                    $resCkTk = COK;
                    $error = 0;
                    $resp = [CTOKEN => $token];
                } else {
                    $resp = [DETAIL => $resp[CMSJ]];
                    $resCkTk = FAIL;
                }
                $str->response($error, $resCkTk, $resp, $tipo);
            }
        break;

        case "posicionesJugador":

            //validamos el token
            if ($str->validaToken($datos_valida_token)) {

                $ju = new Jugadores();
                $arrayInformacion = $ju->posicionesJugador();

                $data = [CRESP => $arrayInformacion];
                $str->response(0, 'OK', $data, $tipo);
            } else {
                die();
            }

        break;

        default:
            $resp = [DETAIL => "Servicio no permitido."];
            $mensaje = FAIL;
            response($error, $mensaje, $resp, "NOSERVICE", $tipo);
            break;
    }
} else {
    $resp = [DETAIL => "Metodo no permitido."];
    $mensaje = FAIL;
    $str->response($error, $mensaje, $resp, "NOMETHOD", $tipo);
}
