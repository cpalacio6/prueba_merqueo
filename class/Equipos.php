<?php

namespace clases;

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/BaseDatos.php';
require_once VENDOR . '/autoload.php';

use clases\BaseDatos;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Equipos extends BaseDatos
{
    // Constructor
    public function __construct()
    {
    }

    public function subirArchivo($post, $files, $uploadFileDir = 'images/', $op = 1)
    {
        $mensaje = '';
        $error = 1;
        $_FILES = $files;

        if (isset($_FILES['uploadedFile']) && $_FILES['uploadedFile']['error'] === UPLOAD_ERR_OK) {

            $fileTmpPath = $_FILES['uploadedFile']['tmp_name'];
            $fileName = $_FILES['uploadedFile']['name'];
            $fileSize = $_FILES['uploadedFile']['size'];
            $fileType = $_FILES['uploadedFile']['type'];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));

            $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

            if($op == 1){
               $allowedfileExtensions = array('jpg', 'png', 'jpeg'); 
            }else{
                $allowedfileExtensions = array('csv', 'xls', 'xlsx');
            }
            
            if (in_array($fileExtension, $allowedfileExtensions)) {
                $dest_path = $uploadFileDir . $newFileName;
                
                if(move_uploaded_file($fileTmpPath, $dest_path))
                {
                    $mensaje ='Archivo cargado correctamente.';
                    $error = 0;

                    if($op == 1){
                        $nombre_pais = $post['nombre_pais'];
                        $bandera = $dest_path;

                        $this->agregarEquipo($nombre_pais, $bandera);
                    }else{
                        $spreadsheet = IOFactory::load(HOME . '/' .$dest_path);
                        $activeSheet = $spreadsheet->setActiveSheetIndex(0);
                        $datosExcel = $activeSheet->toArray();

                        return $this->guardarListaEquipos($datosExcel);
                    }
                }
                else
                {
                    $mensaje = 'El archivo no pudo subir, revise los permisos para cargar la imagen.';
                }
            }else{
                $mensaje = "Error: el formato del archivo no es válido (jpg, png, jpeg)";
            }
        }else{
            $mensaje = 'El archivo no pudo subir, por favor probar con otra imagen.';
        }

        return ['error' => $error, 'mensaje' => $mensaje];
    }

    public function agregarEquipo($nombre_pais, $bandera)
    {
        $insert = "INSERT INTO equipos(nombre_pais, bandera) VALUES('$nombre_pais','$bandera');";
        $this->query($insert);
    }

    public function equipos()
    {
        $sql2 = "SELECT * FROM equipos ORDER BY nombre_pais;";
        $resp2 = $this->query($sql2);

        return $resp2;
    }

    public function guardarListaEquipos($datosExcel)
    {
        $primeraLinea = true;

        foreach ($datosExcel as $key => $value) {
            if ($primeraLinea) {
                $primeraLinea = false;
                continue;
            }

            $bandera = '';

            $this->agregarEquipo($value[0], $bandera);
        }

        return "Equipo(s) cargado(s) exitosamente.";
    }
}