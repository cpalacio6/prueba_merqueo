<?php

namespace clases;

date_default_timezone_set("America/Bogota");

/**
 * Class BaseDatos
 * @package clases
 */
class BaseDatos
{
    private $host = 'localhost';
    private $user = 'root';
    private $pass = '';
    private $bd = 'eliminatorias_mundial';
    private $sql = '';
    private $conn;

    private $active = false;

    /**
     * @return false|\mysqli
     */
    private function connect()
    {
        $mysqli = mysqli_connect($this->host, $this->user, $this->pass, $this->bd);
        if (mysqli_connect_errno($mysqli)) {
            die('Error de Conexión (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }

        mysqli_set_charset($mysqli, "utf8");
        return $mysqli;
    }

    /**
     *
     */
    private function setCon()
    {
        $this->conn = $this->connect();
        $this->active = true;
    }

    /**
     * @param $sql
     * @param int $user
     * @return array
     */
    public function query($sql, $user = null)
    {
        //Eliminando saltos de linea
        $sql = str_replace("\n", "", $sql);

        //Ejecutando query
        $this->setCon();
        //Colocando time_zone -5
        mysqli_query($this->conn, "SET time_zone='-05:00';");
        $result = mysqli_query($this->conn, $sql);
        if (!$result) {
            error_log('Problemas en consulta.  Error: ' . $sql . " " . mysqli_error($this->conn));
            die('Problemas en consulta.  Error: ' . $sql . " " . mysqli_error($this->conn));
        }

        //Borrando conexion
        $this->diconnect();

        //Devolver resultado como array
        $resp = array();
        if (!is_bool($result)) {
            while ($row = mysqli_fetch_assoc($result)) {
                $resp[] = $row;
            }
        } else {
            $resp = ["msg" => "err"];
        }

        return $resp;
    }

    public function obtenerIP()
    {
        $ip = "";
        if (isset($_SERVER)) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        } else {
            if (getenv('HTTP_CLIENT_IP')) {
                $ip = getenv('HTTP_CLIENT_IP');
            } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
                $ip = getenv('HTTP_X_FORWARDED_FOR');
            } else {
                $ip = getenv('REMOTE_ADDR');
            }
        }
        // En algunos casos muy raros la ip es devuelta repetida dos veces separada por coma
        if (strstr($ip, ',')) {
            $ip = array_shift(explode(',', $ip));
        }
        return $ip;
    }

    /**
     * @return bool
     */
    private function diconnect()
    {
        if (!$this->active) {
            return true;
        }
        $this->active = false;
        mysqli_close($this->conn);
        return true;
    }
}
