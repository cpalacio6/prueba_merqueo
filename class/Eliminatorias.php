<?php

namespace clases;

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/BaseDatos.php';
require_once CLASES . '/EquiposService.php';

use clases\BaseDatos;
use clases\EquiposService;

class Eliminatorias extends BaseDatos
{
    // Constructor
    public function __construct()
    {
    }

    /**
     * Método encargado de consultar los equipos visitantes
     *
     * @param Int $op 
     * @param Int $equipo
     * @return Array
     */
    public function equiposVisitantes($op, $equipo)
    {
        if ($op == 1){
            $sql = "SELECT * FROM equipos WHERE id <> $equipo AND id not in (SELECT id_equipo FROM eliminados);";
            $equipos_visitantes = $this->query($sql);
            $retorno = isset($equipos_visitantes[0]['id']) ? $equipos_visitantes[0]['id'] : '';
        }else{
            $sql = "SELECT * FROM equipos WHERE id not in ($equipo) AND id not in (SELECT id_equipo FROM eliminados);";
            $retorno = $this->query($sql)[0]['id'];
        }
        
        return $retorno;
    }

    /**
     * Método encargado de consultar los equipos eliminados
     * 
     * @param Int $equipo_local
     * @return Array
     */
    public function equiposEliminados($str, $op)
    {
        $where = '';

        if($op != 1){
            $where = " WHERE id_equipo IN ($str)";
        }

        $sql = "SELECT * FROM eliminados $where;";
        return $this->query($sql);

    }

    /**
     * Método encargado de verificar si los equipos ya jugaron en esa misma ronda
     * @param Int $ronda
     * @param Int $equipo
     * 
     * @return Array
     */
    public function verificarSiLosEquiposJugaron($ronda, $equipo)
    {
        $query = "SELECT * FROM encuentros WHERE ronda = $ronda AND (id_primer_equipo = $equipo OR id_seg_equipo = $equipo);";
        $respuesta = $this->query($query);

        return $respuesta;
    }

    /**
     * Método encargado de verificar goles y tarjetas historicas
     * @param Int $equipo
     * 
     * @return Array
     */
    public function verificarGolesYTarjetas($equipo)
    {
        $sql_local = "SELECT SUM(goles) as goles, SUM(tarjetas_amarillas) as tarjetas_amarillas, SUM(tarjetas_rojas) as tarjetas_rojas 
        FROM resultados WHERE id_equipo = $equipo;";

        return $this->query($sql_local);
    }

    /**
     * Método encargado de validar cual es el equipo perdedor
     * @param Array $array
     * 
     * @return Int
     */
    public function equipoPerdedor($array)
    {
        //Verificamos que equipo fue el perdedor
        $goles_local = $array['goles_local'];
        $goles_visitante = $array['goles_visitante'];
        $equipo_visitante = $array['equipo_visitante'];
        $equipo_local = $array['equipo_local'];
        $ta_local = $array['ta_local'];
        $ta_visitante = $array['ta_visitante'];
        $tr_local = $array['tr_local'];
        $tr_visitante = $array['tr_visitante'];
        $ronda = $array['ronda'];
        $equipo_eliminado = 0;

        if($goles_local > $goles_visitante){
            $equipo_eliminado = $equipo_visitante;
        }elseif($goles_local < $goles_visitante){
            $equipo_eliminado = $equipo_local;
        }elseif($goles_local == $goles_visitante){
            #Validamos resultados anteriormente realizados

            //Verificamos estadisticas del equipo local
            $resp_local = $this->verificarGolesYTarjetas($equipo_local);
            $total_goles_local = $resp_local[0]['goles'];
            $total_ta_local = $resp_local[0]['tarjetas_amarillas'];
            $total_tr_local = $resp_local[0]['tarjetas_rojas'];

            //Verificamos estadisticas del equipo visitante
            $resp_visitante = $this->verificarGolesYTarjetas($equipo_visitante);;

            $total_goles_visitante = $resp_visitante[0]['goles'];
            $total_ta_visitante = $resp_visitante[0]['tarjetas_amarillas'];
            $total_tr_visitante = $resp_visitante[0]['tarjetas_rojas'];

            //Se compara historial de goles
            if($total_goles_local > $total_goles_visitante){
                $equipo_eliminado = $equipo_visitante;
            }elseif($total_goles_local < $total_goles_visitante){
                $equipo_eliminado = $equipo_local;
            }elseif($total_goles_local == $total_goles_visitante){

                //Se compara historial de tarjetas amarillas
                if($total_ta_local > $total_ta_visitante){
                    $equipo_eliminado = $equipo_visitante;
                }elseif($total_ta_local < $total_ta_visitante){
                    $equipo_eliminado = $equipo_local;
                }elseif($total_ta_local == $total_ta_visitante){

                    //Se compara historial de tarjetas rojas
                    if($total_tr_local > $total_tr_visitante){
                        $equipo_eliminado = $equipo_visitante;
                    }elseif($total_tr_local < $total_tr_visitante){
                        $equipo_eliminado = $equipo_local;
                    }elseif($total_tr_local == $total_tr_visitante){
                        //Si el marcador sigue igualado, se procede a verificar las tarjetas del partido

                        //Se valida tarjetas amarillas del partido
                        if($ta_local > $ta_visitante){
                            $equipo_eliminado = $equipo_visitante;
                        }elseif($ta_local < $ta_visitante){
                            $equipo_eliminado = $equipo_local;
                        }elseif($ta_local == $ta_visitante){

                            //Se valida tarjetas rojas del partido
                            if($tr_local > $tr_visitante){
                                $equipo_eliminado = $equipo_visitante;
                            }elseif($tr_local < $tr_visitante){
                                $equipo_eliminado = $equipo_local;
                            }elseif($tr_local == $tr_visitante){
                                //Si el marcador sigue igualado, se procede a realizar los penaltis

                                $penaltis_local = rand(0, 11);
                                $penaltis_visitante = rand(0, 11);
                                while ($penaltis_local == $penaltis_visitante){
                                    $penaltis_local = rand(0, 11);
                                    $penaltis_visitante = rand(0, 11);
                                }

                                //Se valida quien gana en penaltis
                                if($penaltis_local > $penaltis_visitante){
                                    $equipo_eliminado = $equipo_visitante;
                                }else{
                                    $equipo_eliminado = $equipo_local;
                                }
                            }
                        }
                    }
                }
            }
        }

        //Se procede a la inserción del equipo eliminado
        $insert_eliminados = "INSERT INTO eliminados(ronda, id_equipo) VALUES($ronda, $equipo_eliminado)";
        $this->query($insert_eliminados);

        return $equipo_eliminado;
    }

    /**
     * Método encargado de guardar la cabecera del encuentro
     * @param Int $ronda
     * @param Int $equipo_local
     * @param Int $equipo_visitante
     * @param Int $equipo_eliminado
     * 
     * @return Boolean
     */

    public function guardarEncuentro($ronda, $equipo_local, $equipo_visitante, $equipo_eliminado)
    {
        $insert = "INSERT INTO encuentros(ronda, id_primer_equipo, id_seg_equipo, equipo_eliminado) 
        VALUES($ronda, $equipo_local, $equipo_visitante, $equipo_eliminado);";
        return $this->query($insert);
    }

    /**
     * Método encargado de guardar los resultados del encuentro
     * @param Int $ronda
     * @param Int $equipo
     * @param Int $goles
     * @param Int $tarjetas_am
     * @param Int $tarjetas_ro
     * 
     * @return Boolean
     */
    public function guardarResultados($ronda, $equipo, $goles, $tarjetas_am, $tarjetas_ro)
    {
        $insert_local = "INSERT INTO resultados(id_ronda, id_equipo, goles, tarjetas_amarillas, tarjetas_rojas) 
        VALUES($ronda, $equipo, $goles, $tarjetas_am, $tarjetas_ro)";
        return $this->query($insert_local);
    }

    /**
     * Método encargado de realizar las eliminatorias
     * 
     * @return Array
     */
    public function realizarEliminatoria()
    {
        $eq = new EquiposService();
        $equipos = $eq->equipos()['datos']['resp'];
        $ronda = 1;
        $cantidad_equipos = count($equipos);
        $cantidad_finalizados = 0;

        while ($ronda <= $cantidad_equipos) {

            $c = 1;

            //Se recorren los equipos inscritos
            foreach ($equipos as $key => $value) {
                $equipo_local = $value['id'];

                //Se consulta al equipo visitante
                $equipo_visitante = $this->equiposVisitantes(1, $equipo_local);

                //Se verifica si los equipos ya han sido eliminados
                $str = $equipo_local . ',' .  $equipo_visitante;
                $str = trim($str, ',');
                $resp_eliminado = $this->equiposEliminados($str, 2);

                $continue = 0;

                //Se valida si alguno de los equipos fue eliminado, en caso de ser verdadero se procede a seleccionar un nuevo equipo
                if(count($resp_eliminado) > 0){
                    foreach ($resp_eliminado as $key2 => $value2) {
                        if($value2['id_equipo'] == $equipo_local){
                            $continue = 1;
                        }
                    }

                    if($continue == 1) { 
                        continue;
                    }; 
                }

                //Validar si el equipo local ya jugó anteriormente en esa misma ronda
                $resp_p_equipo = $this->verificarSiLosEquiposJugaron($ronda, $equipo_local);

                //Si el equipo local ya jugó, se procede a tomar un nuevo equipo
                if(count($resp_p_equipo) > 0){
                    continue;
                }

                //Validar si el equipo visitante ya jugó anteriormente en esa misma ronda
                $resp_s_equipo = $this->verificarSiLosEquiposJugaron($ronda, $equipo_visitante);
                $str = $equipo_visitante;
                $break = 0;

                //Si el equipo visitante ya jugó en esta ronda, se procede a seleccionar un nuevo contrincante
                while ((count($resp_s_equipo) > 0) || ($equipo_local == $equipo_visitante)){
                    $str = trim($str, ',');
                    $equipo_visitante = $this->equiposVisitantes(2, $str);

                    if($equipo_visitante == '' || $equipo_visitante == 0){
                        break;
                    }

                    $str =  $str . ',' . $equipo_visitante;
                    $resp_s_equipo = $this->verificarSiLosEquiposJugaron($ronda, $equipo_visitante);
                }

                //Se escoge al azar cuantos goles hizo cada equipo, asi mismo realizamos el proceso con las tarjetas
                $goles_local = rand(0, 10);
                $goles_visitante = rand(0, 10);

                $tarjetas_am_local = rand(0, 10);
                $tarjetas_am_visitante = rand(0, 10);

                $tarjetas_ro_local = rand(0, 5);
                $tarjetas_ro_visitante = rand(0, 5);

                //Se arma el array para proceder con la verificación del equipo perdedor
                $array = [
                    'goles_local' => $goles_local,
                    'goles_visitante' => $goles_visitante,
                    'equipo_local' => $equipo_local,
                    'equipo_visitante' => $equipo_visitante,
                    'ta_local' => $tarjetas_am_local,
                    'tr_local' => $tarjetas_ro_local,
                    'ta_visitante' => $tarjetas_am_visitante,
                    'tr_visitante' => $tarjetas_ro_visitante,
                    'ronda' => $ronda
                ];

                //Se procede a validar cual es el equipo perdedor e insertamos en la base de datos
                $equipo_eliminado = $this->equipoPerdedor($array);

                //Se procede a guardar el encuentro realizado
                $this->guardarEncuentro($ronda, $equipo_local, $equipo_visitante, $equipo_eliminado);

                //Se procede a guardar el resultado del equipo local
                $this->guardarResultados($ronda, $equipo_local, $goles_local, $tarjetas_am_local, $tarjetas_ro_local);

                //Se procede a guardar el resultado del equipo visitante
                $this->guardarResultados($ronda, $equipo_visitante, $goles_visitante, $tarjetas_am_visitante, $tarjetas_ro_visitante); 

                $c++;
            }

            if($c == 2){
                break;
            }

            $ronda++;
        }

        return "OK";
    }

    public function enfrentamientosTotales()
    {
        $sql = "SELECT en.ronda, eq.id as id_local, eq.nombre_pais as local, eq2.id as id_visitante, eq2.nombre_pais as visitante, 
                eq3.nombre_pais as equipo_eliminado FROM encuentros en 
                INNER JOIN equipos eq ON eq.id = en.id_primer_equipo 
                INNER JOIN equipos eq2 ON eq2.id = en.id_seg_equipo 
                INNER JOIN equipos eq3 ON eq3.id = en.equipo_eliminado ";

        return $this->query($sql);
    }

    public function resultadoSegunRonda($id_ronda, $id_equipo)
    {
        $sql = "SELECT * FROM resultados WHERE id_ronda = '$id_ronda' AND id_equipo = '$id_equipo';";
        return $this->query($sql);
    }

    public function campeon()
    {
        $sql = "SELECT * FROM equipos WHERE id NOT IN (SELECT equipo_eliminado FROM encuentros);";
        return $this->query($sql);
    }

    public function resultadoGeneral($id_equipo)
    {
        $sql = "SELECT SUM(r.goles) as goles_totales, SUM(r.tarjetas_amarillas) as tarjetas_amarillas_totales, 
        SUM(r.tarjetas_rojas) as tarjetas_rojas_totales FROM resultados r WHERE r.id_equipo = '$id_equipo';";

        return $this->query($sql);
    }
}