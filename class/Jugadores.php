<?php

namespace clases;

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/BaseDatos.php';
require_once VENDOR . '/autoload.php';

use clases\BaseDatos;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Jugadores extends BaseDatos
{
    // Constructor
    public function __construct()
    {
    }

    public function subirArchivo($post, $files, $uploadFileDir = 'images/', $op = 1)
    {
        $mensaje = '';
        $error = 1;
        $_FILES = $files;

        if (isset($_FILES['uploadedFile']) && $_FILES['uploadedFile']['error'] === UPLOAD_ERR_OK) {

            $fileTmpPath = $_FILES['uploadedFile']['tmp_name'];
            $fileName = $_FILES['uploadedFile']['name'];
            $fileSize = $_FILES['uploadedFile']['size'];
            $fileType = $_FILES['uploadedFile']['type'];
            $fileNameCmps = explode(".", $fileName);
            $fileExtension = strtolower(end($fileNameCmps));

            $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

            if($op == 1){
               $allowedfileExtensions = array('jpg', 'png', 'jpeg'); 
            }else{
                $allowedfileExtensions = array('csv', 'xls', 'xlsx');
            }

            if (in_array($fileExtension, $allowedfileExtensions)) {
                $dest_path = $uploadFileDir . $newFileName;
                
                if(move_uploaded_file($fileTmpPath, $dest_path))
                {
                    $mensaje ='Archivo cargado correctamente.';
                    $error = 0;
                    
                    if($op == 1){
                        $foto_jugador = $dest_path;
                        $this->agregarJugador($post, $foto_jugador); 
                    }else{
                        $spreadsheet = IOFactory::load(HOME . '/' . $dest_path);
                        $activeSheet = $spreadsheet->setActiveSheetIndex(0);
                        $datosExcel = $activeSheet->toArray();

                        return $this->guardarListaJugadores($datosExcel);
                    }
                }
                else
                {
                    $mensaje = 'El archivo no pudo subir, revise los permisos para cargar la imagen.';
                }
            }else{
                $mensaje = "Error: el formato del archivo no es válido (jpg, png, jpeg)";
            }
        }else{
            $mensaje = 'El archivo no pudo subir, por favor probar con otra imagen.';
        }

        return ['error' => $error, 'mensaje' => $mensaje];
    }

    private function agregarJugador($post, $foto_jugador)
    {
        $nombre_jugador = $post['nombre_jugador'];
        $apellidos_jugador = $post['apellidos_jugador'];
        $nacionalidad_jugador = $post['nacionalidad_jugador'];
        $edad_jugador = $post['edad_jugador'];
        $posicion_jugador = $post['posicion_jugador'];
        $numero_camisa_jugador = $post['numero_camisa_jugador'];

        $insert = "INSERT INTO jugadores(id_equipo, nombre_jugador, apellidos_jugador, edad, posicion, numero_camiseta, foto_jugador) 
        VALUES('$nacionalidad_jugador','$nombre_jugador','$apellidos_jugador','$edad_jugador','$posicion_jugador','$numero_camisa_jugador'
        ,'$foto_jugador');";
        
        $this->query($insert);
    }

    public function posicionesJugador()
    {
        $sql = "SELECT * FROM posiciones_jugadores ORDER BY posicion;";
        $resp = $this->query($sql);
        
        return $resp;
    }

    public function guardarListaJugadores($datosExcel)
    {
        $primeraLinea = true;

        foreach ($datosExcel as $key => $value) {
            if ($primeraLinea) {
                $primeraLinea = false;
                continue;
            }

            $foto_jugador = '';
            $array = [
                'nombre_jugador' => $value[1],
                'apellidos_jugador' => $value[2],
                'nacionalidad_jugador' => $value[0],
                'edad_jugador' => $value[3],
                'posicion_jugador' => $value[4],
                'numero_camisa_jugador' => $value[5]
            ];

            $this->agregarJugador($array, $foto_jugador);
        }

        return "Jugador(es) cargado(s) exitosamente.";
    }
}