<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/Eliminatorias.php';
require_once CLASES . '/EquiposService.php';

use clases\Eliminatorias;
use clases\EquiposService;

$el = new Eliminatorias();
$eq = new EquiposService();

$equipos = $eq->equipos();
$error_servicio_eq = $equipos['error'];

if($error_servicio_eq == 1){
    die($equipos['mensaje']);
}else{
    $cantidad_equipos = count($equipos['datos']['resp']);
}


if(!in_array($cantidad_equipos, [8, 16, 32])){
    die("Los equipos no están compelos, por favor deben ser 8, 16 o 32 equipos.");
}else{
    
    $enfrentamientos = $el->enfrentamientosTotales();

    if(count($enfrentamientos) == 0){
        $el->realizarEliminatoria();
        $enfrentamientos = $el->enfrentamientosTotales();
    }

    $campeon = $el->campeon()[0]['nombre_pais'];

    echo "<a href='index.php'>Regresar al menu</a>";

    echo "<h1>¡$campeon CAMPEON!</h1> <br><br>";

    echo "<h3>Tabla de encuentros y resultados</h3>";
    echo '<table border=1>';

    echo '<tr> <td>Ronda</td> <td>Local(L)</td> <td>Visitante(V)</td> <td>Resultado(L-V)</td> <td>Tarjetas Amarillas(L-V)</td> 
    <td>Tarjetas rojas(L-V)</td> <td>Eliminado</td>  </tr>';

    foreach ($enfrentamientos as $key => $value) {

        
        $resultado_local = $el->resultadoSegunRonda($value['ronda'], $value['id_local']);
        $goles_local = $resultado_local[0]['goles'];
        $ta_local = $resultado_local[0]['tarjetas_amarillas'];
        $tr_local = $resultado_local[0]['tarjetas_rojas'];

        $resultado_visitante = $el->resultadoSegunRonda($value['ronda'], $value['id_visitante']);
        $goles_visitante = $resultado_visitante[0]['goles'];
        $ta_visitante = $resultado_visitante[0]['tarjetas_amarillas'];
        $tr_visitante = $resultado_visitante[0]['tarjetas_rojas'];

        echo '<tr> <td>'.$value['ronda'].'</td> <td>'.$value['local'].'</td> <td>'.$value['visitante'].'</td> 
        <td>('.$goles_local.'-'.$goles_visitante.')</td> <td>('.$ta_local.'-'.$ta_visitante.')</td> <td>('.$tr_local.'-'.$tr_visitante.')</td> 
        <td>'.$value['equipo_eliminado'].'</td> </tr>';
    }

    echo '</table>';

    echo "<br><br><h3>Tabla de estadistica en todo el campeonato</h3>";

    echo '<table border=1>';

    echo '<tr> <td>Pais</td> <td>Goles totales</td> <td>Tarjetas Amarillas totales</td> 
    <td>Tarjetas rojas totales</td> </tr>';

    foreach ($equipos['datos']['resp'] as $key => $value) {

        $estadistica_general = $el->resultadoGeneral($value['id'])[0];

        echo '<tr> <td>'.$value['nombre_pais'].'</td> <td>'.$estadistica_general['goles_totales'].'</td> <td>'.$estadistica_general['tarjetas_amarillas_totales'].'</td> 
            <td>'.$estadistica_general['tarjetas_rojas_totales'].'</td> </tr>';
    }

    echo '</table>';

}