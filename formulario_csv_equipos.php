<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/Equipos.php';

use clases\Equipos;

$eq = new Equipos();


?>

<form method="POST" action="index.php" enctype="multipart/form-data">
<input type="hidden" name="excel" id="excel" value="1">
    <table>
        <tr>
            <td>Subir excel: </td>
            <td><input type="file" name="uploadedFile" /></td>
        </tr> 
        <tr>
            <td colspan="2"><input type="submit" name="btn_form" value="Cargar" /></td>
        </tr>
    </table>
</form>

<a href="index.php">Regresar al menu</a>

<div style="position:relative; top:10%;">

<?php
if(isset($_POST['excel']) && $_POST['excel'] == 1){
    $datosExcel = $eq->subirArchivo($_POST, $_FILES, "uploads/", 2);
    echo $datosExcel;
}
?>

</div>

