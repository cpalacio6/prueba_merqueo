<?php

namespace clases;

require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/firebase/php-jwt/src/JWT.php';
require_once CLASES . '/BaseDatos.php';

use Firebase\JWT\JWT;
use clases\BaseDatos;

class TokenJWT extends JWT
{
    private static $secret_key = 'El1m1n@t0r14s*';
    private static $encrypt = ['HS256'];
    private static $aud = null;

    public function SignIn($data = [])
    {
        $time = time();

        $token = array(
            'iat' => $time, // Tiempo que inició el token
            'exp' => $time + (60 * 60), // Tiempo que expirará el token (+1 hora)
            //'exp' => $time + (60), // Tiempo que expirará el token (+1 minuto)
            'aud' => self::Aud(),
            'data' => $data // información del usuario
        );

        return TokenJWT::encode($token, self::$secret_key);
    }

    public function Check($token)
    {
        if (empty($token)) {
            return "Invalid token supplied.";
        }

        try {
            $decode = TokenJWT::decode(
                $token,
                self::$secret_key,
                self::$encrypt
            );

            if ($decode->aud !== self::Aud()) {
                return "Invalid user logged in.";
            } else {
                return $decode;
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function Aud()
    {
        $aud = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }

        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();

        return sha1($aud);
    }
}