<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/BaseDatos.php';

use clases\BaseDatos;

$e = new BaseDatos();

?>

<form method="POST" action="upload.php" enctype="multipart/form-data">
    <table>
        <tr>
            <td>Nombre del pais: </td>
            <td><input type="text" name="nombre_pais" id="nombre_pais"></td>
        </tr>
        <tr>
            <td>Bandera: </td>
            <td><input type="file" name="uploadedFile" /></td>
        </tr> 
        <tr>
            <td colspan="2"><input type="submit" name="btn_form" value="Cargar" /></td>
        </tr>
    </table>
</form>

<br>

<a href="index.php">Regresar al menu</a>
