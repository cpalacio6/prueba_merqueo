<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/prueba/config/rutas.php';
require_once CLASES . '/BaseDatos.php';
require_once CLASES . '/JugadoresService.php';
require_once CLASES . '/EquiposService.php';

use clases\BaseDatos;
use clases\JugadoresService;
use clases\EquiposService;

$bd = new BaseDatos();
$ju = new JugadoresService();
$eq = new EquiposService();

//Se consume el servicio para traer la posición del jugador
$posicion_jugador = $ju->posicionesJugador();

$error_servicio_pj = $posicion_jugador['error'];

if($error_servicio_pj == 1){
    die($posicion_jugador['mensaje']);
}else{
    $option_posicion = "";

    foreach ($posicion_jugador['datos']['resp'] as $key => $value) {
        $id = $value['id'];
        $posicion = $value['posicion'];
        $option_posicion = $option_posicion . "<option value='$id'>$posicion</option>";
    }
}

$equipos = $eq->equipos();
$error_servicio_eq = $equipos['error'];

if($error_servicio_eq == 1){
    die($equipos['mensaje']);
}else{
    $option_pais = "";

    foreach ($equipos['datos']['resp'] as $key2 => $value2) {
        $id2 = $value2['id'];
        $nombre_pais = $value2['nombre_pais'];
        $option_pais = $option_pais . "<option value='$id2'>$nombre_pais</option>";
    }
}

?>

<form method="POST" action="upload.php" enctype="multipart/form-data">
    <table>
        <tr>
            <td>Nombre del jugador: </td>
            <td><input type="text" name="nombre_jugador" id="nombre_jugador"></td>
        </tr>
        <tr>
            <td>Apellidos del jugador: </td>
            <td><input type="text" name="apellidos_jugador" id="apellidos_jugador"></td>
        </tr>
        <tr>
            <td>Nacionalidad: </td>
            <td>
                <select name="nacionalidad_jugador" id="nacionalidad_jugador">
                    <option value="">Seleccione</option>
                    <?= $option_pais ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Edad: </td>
            <td><input type="text" name="edad_jugador" id="edad_jugador"></td>
        </tr>
        <tr>
            <td>Posición del jugador: </td>
            <td>
                <select name="posicion_jugador" id="posicion_jugador">
                    <option value="">Seleccione</option>
                    <?= $option_posicion ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Número de camiseta del jugador: </td>
            <td><input type="text" name="numero_camisa_jugador" id="numero_camisa_jugador"></td>
        </tr>
        <tr>
            <td>Foto del jugador: </td>
            <td><input type="file" name="uploadedFile" /></td>
        </tr> 
        <tr>
            <td colspan="2"><input type="submit" name="btn_form" value="Cargar" /></td>
        </tr>
    </table>
</form>

<br>

<a href="index.php">Regresar al menu</a>