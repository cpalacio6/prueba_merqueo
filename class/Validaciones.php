<?php

namespace clases;

class Validaciones
{
    //constantes generales
    const CESTADO = 'estado';
    const CMSJ = 'mensaje';
    const CDATOS = 'datos';

    public function __construct()
    {
    }

    /**
     * @param $tipo
     * @param $campo
     * @param $valor
     * @param bool $obligatorio
     * @return array
     */
    public function tipoDato($tipo, $campo, $valor, $obligatorio = false)
    {
        $tipo = trim($tipo);
        $campo = mb_strtoupper(trim($campo));
        $valor = trim($valor);
        $resultado = [$this::CESTADO => 1, $this::CMSJ => 'OK'];

        if (empty($valor) && $valor == "" && $obligatorio) {
            $resultado[$this::CESTADO] = 2;
            $resultado[$this::CMSJ] = ("El campo $campo no puede ser vacío.");
        } else {
            switch ($tipo) {
                case "int":
                    if (!ctype_digit($valor)) {
                        $resultado[$this::CESTADO] = 2;
                        $resultado[$this::CMSJ] = "El campo $campo debe ser entero.";
                    }

                    break;

                case "double":

                    if (!is_numeric($valor)) {
                        $resultado[$this::CESTADO] = 2;
                        $resultado[$this::CMSJ] = "El campo $campo debe ser un digito.";
                    }

                    break;

                case 'string':

                    if (!is_string($valor)) {
                        $resultado[$this::CESTADO] = 2;
                        $resultado[$this::CMSJ] = "El campo $campo debe ser un digito.";
                    }

                    break;

                case 'bool':

                    if (!is_bool($valor) && $valor != 0 && $valor != 1) {
                        $resultado[$this::CESTADO] = 2;
                        $resultado[$this::CMSJ] = "El campo $campo debe ser booleano.";
                    }

                    break;

                case 'date':

                    //los fecha tiene que ir en mes-dia-año separadas por /
                    $fecha = explode('/', $valor);

                    if (!checkdate($fecha[0], $fecha[1], $fecha[2])) {
                        $resultado[$this::CESTADO] = 2;
                        $resultado[$this::CMSJ] = "El campo $campo debe ser booleano.";
                    }

                    break;

                case 'boolean':
                    //Booleano para servicios

                    if (
                        $valor !== '0' &&
                        $valor !== '1' &&
                        $valor !== 'true' &&
                        $valor !== 'false'
                    ) {
                        $resultado[$this::CESTADO] = 2;
                        $resultado[$this::CMSJ] = "El campo $campo debe ser booleano.";
                    }

                    break;
            }
        }
        //        error_log("Resultado de $campo: " . $resultado["estado"] . " - Valor: $valor");

        return $resultado;
    }

    /**
     * Método encargado de validar los arrays
     * 
     * @param Array $datos_validar, $informacion_datos
     *  
     * @return Array
     */
    function validaArrays($datos_validar, $informacion_datos)
    {
        //variable de interrupcion
        $breaker = false;

        //Se inicializa la respuesta en ok
        $respuesta = [
            $this::CESTADO => 1,
            $this::CMSJ => 'OK'
        ];

        //Se valida que lleguen datos al array
        if (count($datos_validar) > 0) {
            //Se recorre el array
            foreach ($datos_validar as $clave => $valor) {

                //Se valida si se debe romper el ciclo
                if ($breaker) {
                    break;
                }

                //Se valida si es un objeto o si es un array
                if (is_array($valor) || is_object($valor)) {

                    //De serlo, se llama a si mismo para continuar el ciclo
                    $validacion_hijo = $this->validaArrays($valor, $informacion_datos);
                    $estado_validado = $validacion_hijo[$this::CESTADO];

                    $respuesta[$this::CDATOS][$clave] = $validacion_hijo[$this::CDATOS];

                    if ($estado_validado != 1) {
                        $breaker = true;
                        $respuesta = [
                            $this::CESTADO => $validacion_hijo[$this::CESTADO],
                            $this::CMSJ => $validacion_hijo[$this::CMSJ],
                            $this::CDATOS => []
                        ];
                    }
                } else {

                    //Se almacena el valor actual y se limpia
                    $valor_actual = trim(addslashes($valor));

                    //Se valida el dato
                    $validacion = $this->tipoDato($informacion_datos[$clave][0], $clave, $valor_actual, true);

                    /*Se valida si el estado de lo validado es distinto de correcto, 
                    si es obligatorio validar el campo, si el valor es distinto de 
                    vacío y de cero*/
                    if (
                        ($validacion[$this::CESTADO] != 1 && $valor_actual != '') ||
                        ($informacion_datos[$clave][1] == 1 && $valor_actual == '')
                    ) {

                        $breaker = true;

                        //Se cambia los valores de la respuesta 
                        $respuesta = [
                            $this::CESTADO => $validacion[$this::CESTADO],
                            $this::CMSJ => $validacion[$this::CMSJ],
                            $this::CDATOS => []
                        ];
                    } else {
                        //Se agregan los valores validados al array de respuesta
                        $respuesta[$this::CDATOS][$clave] = $valor_actual;
                    }
                }
            }
        } else {
            $respuesta = [
                $this::CESTADO => 2,
                $this::CMSJ => 'El array es vacío, por lo tanto, no se puede validar valores.'
            ]; 
        }
        return $respuesta;
    }

    public function validarDatos($array_datos)
    {
        foreach ($array_datos as $key) {
            $respuesta = $this->tipoDato($key['tipo'], $key['campo'], $key['valor'], $key['obligatorio']);
            if ($respuesta['estado'] == 2) {
                return $respuesta;
            }
        }
        return [$this::CESTADO => 1, $this::CMSJ => 'OK'];
    }
}
